#! /usr/bin/env python

import argparse
import gc
import os
import os.path
import subprocess

from secret import SecretConfigParser

parser = argparse.ArgumentParser()

parser.add_argument(
    '-c', '--config', required=True,
    help='deployment configuration file')

parser.add_argument(
    '-s', '--section', default='default',
    help='deployment configuration file section')

parser.add_argument(
    '-k', '--key', required=True,
    help='deployment configuration secret key file')

args = parser.parse_args()
section = args.section

with open(args.key, 'rb') as f:
    for line in f:
        sparser = SecretConfigParser(secret_key=line.strip())
        break

sparser.read(args.config)
del args
gc.collect()

playbook_path = 'playbooks'
roles_path = os.path.join(playbook_path, 'roles')
requirements = os.path.join(playbook_path, 'requirements.yml')
inventory = os.path.join(playbook_path, 'inventory')
site_yml = os.path.join(playbook_path, 'site.yml')

user = sparser.get(section, 'user')

env = {}
env.update(os.environ)
env.update({
    'ANSIBLE_ROLES_PATH': roles_path,
    'ANSIBLE_HOST_KEY_CHECKING': 'false'
})

subprocess.check_call([
    'ansible-galaxy', 'install',
    '--roles-path=' + roles_path, '-r', requirements
])

ansible_command = ['ansible-playbook', '-v']
if user:
    ansible_command.extend(['--user', user])

ansible_command.extend(['--inventory-file', inventory, site_yml])
# '-e', 'ABC=123',

subprocess.check_call(ansible_command, env=env)

