#! /usr/bin/env python

import contextlib
import gc

from cryptography.fernet import Fernet

try:
    from configparser import SafeConfigParser as ConfigParser
except ImportError:
    from ConfigParser import SafeConfigParser as ConfigParser

class SecretConfigParser(ConfigParser):
    def __init__(self, *args, **kwargs):
        secret_prefix = kwargs.pop('secret_prefix', 'secret://')
        secret_key = kwargs.pop('secret_key')

        super(SecretConfigParser, self).__init__(*args, **kwargs)
        self._crypto = None
        self.key = secret_key
        self.secret_prefix = secret_prefix
        self.secret_prefix_len = len(self.secret_prefix)

    @property
    def crypto(self):
        if self._crypto is None:
            if self.key is None:
                raise ValueError('cannot decrypt secret data without a key')
            self._crypto = Fernet(self.key)
            del self.key
            gc.collect()
        return self._crypto

    def get(self, *args, **kwargs):
        try:
            result = super(SecretConfigParser, self).get(*args, **kwargs)
            if result.startswith(self.secret_prefix):
                path = result[self.secret_prefix_len:]
                result = self.crypto.decrypt(path.encode()).decode()
        except ValueError as e:
            sys.stderr.write('Error in reading secret data:\n')
            sys.stderr.write('\t')
            sys.stderr.write(e.args[0])
            sys.stderr.write('\n')
            sys.exit(1)

        return result

    def __del__(self):
        del self._crypto
        gc.collect()

def write_config(input_file, output_file, crypto):
    for line in input_file:
        if b'=' not in line:
            output_file.write(line)
            continue

        key, val = tuple(x.strip() for x in line.split(b'=', 1))
        if val.startswith(b'secret://'):
            output_file.write(key)
            output_file.write(b' = secret://')
            output_file.write(crypto.encrypt(val))
            output_file.write(b'\n')
        else:
            output_file.write(key)
            output_file.write(b' = ')
            output_file.write(val)
            output_file.write(b'\n')

def unique(iterable):
    visited = set()
    for x in iterable:
        if x in visited:
            continue
        visited.add(x)
        yield x

def read_config(crypto, params):
    for param in params:
        spl = param.split(':', 1)
        if len(spl) == 1:
            section = 'default'
            spl = spl[0]
        else:
            section, spl = spl
        param_list = [x.strip() for x in spl.split(',')]

        print('[{}]'.format(section))
        print('\n'.join(
            '{} = {}'.format(p, crypto.get(section, p))
            for p in unique(param_list)
        ))


if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    subparsers.dest = 'action'
    subparsers.required = True

    create_parser = subparsers.add_parser(
            'create', help='create a secret config file')

    create_parser.add_argument(
            '-i', '--input', help='path to the input config file')

    create_parser.add_argument(
            '-o', '--output', help='path to the output config file')

    create_parser.add_argument(
            '-k', '--key', help='path to the key file', required=True)

    create_parser.add_argument(
            '-c', '--create',
            help="create a new key if the given key file doesn't exist",
            action='store_true', default=False)


    read_parser = subparsers.add_parser(
            'get', help='read data from a secret config file')

    read_parser.add_argument(
            '-i', '--input', help='path to the input config file',
            required=True)

    read_parser.add_argument(
            '-k', '--key', default=None, help='path to the key file')

    read_parser.add_argument(
            'params', nargs='+', help='parameters to read')

    args = parser.parse_args()
    key = args.key

    if args.action == 'create':
        input_file = args.input
        if input_file is None:
            input_file = sys.stdin.buffer
        else:
            input_file = open(input_file, 'rb')

        output_file = args.output
        if output_file is None:
            output_file = sys.stdout.buffer
        else:
            output_file = open(output_file, 'wb')

        with contextlib.ExitStack() as stack:
            try:
                key_file = open(key, 'rb')
            except FileNotFoundError:
                if not args.create:
                    raise

                key_file = open(key, 'wb')
                key = Fernet.generate_key()
                stack.enter_context(key_file)
                key_file.write(key)
                crypto = Fernet(key)
            else:
                stack.enter_context(key_file)
                key = key_file.read()
                crypto = Fernet(key)

            if input_file is not sys.stdin.buffer:
                stack.enter_context(input_file)

            if output_file is not sys.stdout.buffer:
                stack.enter_context(output_file)

            write_config(input_file, output_file, crypto)
    else:
        with contextlib.ExitStack() as stack:
            if key is not None:
                key_file = open(key, 'rb')
                stack.enter_context(key_file)
                for line in key_file:
                    crypto = SecretConfigParser(secret_key=line.strip())
                    break
            else:
                crypto = SecretConfigParser(secret_key=None)

            crypto.read(args.input)

            read_config(crypto, args.params)

