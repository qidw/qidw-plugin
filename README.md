QIDW Girder Plugin
==================

See [docker/README.md](docker/README.md) for developer instructions.

NOTE: Please keep in mind that this plugin is developed on public resources, and
therefore must be free of any private or secret information except for that
which is concealed using adequate data protection measures.  For more
information, please submit an issue.

