
import cherrypy

from girder.utility import setting_utilities

from girder.api import access
from girder.api.describe import autoDescribeRoute, Description
from girder.api.rest import Resource


@setting_utilities.validator({
    'qidw.logoUri'
})
def validateSettings(doc):
    uri = doc['value']
    pass


class QIDW(Resource):
    """QIDW plugin class."""

    def __init__(self):
        """Initialize the QIDW plugin."""
        super(QIDW, self).__init__()
        self.resourceName = 'qidw'
        self.route('GET', ('logo.jpg',), self.getLogo)

    @autoDescribeRoute(
        Description('Fetch the logo for QIDW')
    )
    @access.public
    def getLogo(self):
        raise cherrypy.HTTPRedirect(self.model('setting').get('qidw.logoUri'))


def load(info):
    """QIDW plugin entry point."""
    info['apiRoot'].qidw = QIDW()

