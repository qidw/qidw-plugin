
import { getCurrentUser } from 'girder/auth';
import { wrap } from 'girder/utilities/PluginUtils';
import GlobalNavView from 'girder/views/layout/GlobalNavView';
import HeaderView from 'girder/views/layout/HeaderView';

wrap(GlobalNavView, 'render', function (render) {
  render.call(this);

  // add logo image to nav
  const url = '/api/v1/qidw/logo.jpg';

  this.$('.g-global-nav-main').prepend(
    `<a href="#"><img width=168 src="${url}"` +
    'style="padding-bottom: 6px; ' +
    'border-bottom: 1px solid #eee; ' +
    'margin-bottom: 6px;"></a>'
  );

  // remove users link from nav for non-admins
  const currentUser = getCurrentUser();
  const loggedIn = Boolean(currentUser);
  const isAdmin = (loggedIn && currentUser.get('admin'));

  if (isAdmin) {
    if (this.$('[g-name="g-analytics"]').length === 0) {
      this.$('.g-global-nav').append(
        '<li class="g-global-nav-li"><a class="g-nav-link" ' +
        ' g-name="g-analytics" href="https://analytics.google.com">' +
        ' <i class="icon-signal"></i><span>Google Analytics</span></a></li>'
      );
      this.$('[g-name="g-analytics"]').on('click', (e) => {
        e.stopPropagation();
      });
    }
  } else {
    this.$('[g-target="users"]').remove();
    this.$('[g-target="groups"]').remove();
  }
});

wrap(HeaderView, 'render', function (render) {
  render.call(this);

  // change app title
  this.$('.g-app-title').text('QIDW');

  // change page title
  document.title = 'QIDW | RSNA';
});
